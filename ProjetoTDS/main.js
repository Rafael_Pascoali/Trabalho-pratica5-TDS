function addNewTask(){
    var list = document.getElementById("list");
    var text = document.getElementById("task_name").value;
    var listItem = document.createElement("li");
//Ali em cima tem as variaveis necessarias para pegar o que será colocado na lista
    if(text.length === 0){
        alert("A Tarefa precisa ter pelo menos 1 caracter");
        return;
    }
//Esse if é para se o usuario nao colocar nenhum dado no escopo
    listItem.className = "list-item"
    
    const textElement = document.createTextNode(text);
    listItem.appendChild(textElement)
    list.appendChild(listItem)
//E aqui as variaveis são usadas para coletar o que foi colocado no escopo
}